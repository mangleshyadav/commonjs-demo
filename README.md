# Instruction

your system should have install following tools.
### 1) node 4+ [check intalled version using command " node -v "] ###
### 2) npm 4+ [check intalled version using command " npm -v "] ###

# How to build demo.

 1)  npm build  
 or 
 2)  node result.js


# Description

The CommonJS group defined a module format to solve JavaScript scope issues by making sure each module is executed in its own namespace.

This is achieved by forcing modules to explicitly export those variables it wants to expose to the “universe”, and also by defining those other modules required to properly work.

To achieve this CommonJS gives you two tools:

the require() function, which allows to import a given module into the current scope.
the module object, which allows you to export something from the current scope.

[ ref: https://webpack.github.io/docs/commonjs.html ]

# Project Desc

you can see the 3 files result.js, salute.js , world.js
salute.js  is independent file.
world.js file is dependent on salute.js file.
result.js file is dependent on world.js.

result file is displaying combined output of all.